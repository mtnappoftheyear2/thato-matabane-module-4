import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';




void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override

  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState(){
    super.initState();

    Future.delayed(Duration(seconds: 3)).then((value){
      Navigator.of(context).pushReplacement(CupertinoPageRoute(builder: (ctx) => FirstPage()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Image(image:
            AssetImage("assets/images/hubb.jpg"),
              width: 300,
            ),
            SizedBox(
              height: 50.0,
            ),
          SpinKitFadingCircle(
            color: Colors.orange,
            size: 50.0,
          )
          ],
        ),
      ),
    );
  }
}


class FirstPage extends StatelessWidget{
  @override

  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: Colors.orange,
              height: 100,
              width: 100,
              child: FittedBox(
              fit: BoxFit.fill,
              child: Image.asset('assets/images/cart.jpg',
            ),
            ),
            ),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter username',
              ),
            ),
            Padding( padding: const EdgeInsets.all(8.0), child: FloatingActionButton.extended(onPressed: (){
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context)=>
                SecondPage(),
                ),
              );
            },
              label: const Text('Login'),
              icon: const Icon(Icons.shopping_cart),
              backgroundColor: Colors.orange,
            ),
            ),
            Padding( padding: const EdgeInsets.all(8.0), child: FloatingActionButton.extended(onPressed: (){
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context)=>
                    Register(),
                ),
              );
            },
              label: const Text('Register'),
              icon: const Icon(Icons.account_circle),
              backgroundColor: Colors.orange,
            ),
            ),
          ],
        ),
      ),
    );
  }
}


class Register extends StatelessWidget{
  @override

  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Registration"),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              color: Colors.orange,
              height: 100,
              width: 100,
              child: FittedBox(
                fit: BoxFit.fill,
                child: Image.asset('assets/images/cart.jpg',
                ),
              ),
            ),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Enter username',
              ),
            ),
            Padding( padding: const EdgeInsets.all(8.0), child: FloatingActionButton.extended(onPressed: (){
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context)=>
                    SecondPage(),
                ),
              );
            },
              label: const Text('Register'),
              icon: const Icon(Icons.account_circle),
              backgroundColor: Colors.orange,
            ),
            ),
          ],
        ),
      ),
    );
  }
}

class SecondPage extends StatelessWidget{

  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      body: Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
              child: GridView.count(
                crossAxisCount: 2,
                padding: EdgeInsets.all(3.0),
                children: <Widget>[

                  FloatingActionButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (context)=>
                            ThirdPage(),
                        ),
                      );

                    },
                    child: Container(
                      child: makeDashboardItem("Items", Icons.receipt_long),
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (context)=>
                            MyHomePage(title: 'Add more apples')
                        ),
                      );

                    },
                    child: Container(
                      child: makeDashboardItem("Items", Icons.add),
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(builder: (context)=>
                            UserProfile(),
                        ),
                      );

                    },
                    child: Container(
                      child: makeDashboardItem("Items", Icons.account_circle),
                    ),
                  ),
                ],
              ),
            ),

    );
  }
}

class ThirdPage extends StatelessWidget{
  @override

  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Items in stock now"),
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 100,
                width: 100,
                child: FittedBox(
                  fit: BoxFit.fill,
                  child: Image.asset('assets/images/apple.jpg',
                  ),
                ),
              ),

              Text("You have 10 apples remaining",
              style: TextStyle(fontSize: 28.0),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UserProfile extends StatelessWidget{
  @override

  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 100,
                width: 100,
                child: FittedBox(
                  fit: BoxFit.fill,
                  child: Image.asset('assets/images/profile.jpg',
                  ),
                ),
              ),

              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Full Name',
                ),
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Email',
                ),
              ),

              ElevatedButton(
                onPressed: () {
                  final snackBar = SnackBar(
                    content: const Text('Edits have been saved'),
                    action: SnackBarAction(
                      label: 'Undo',
                      onPressed: () {
                        // Some code to undo the change.
                      },
                    ),
                  );

                  // Find the ScaffoldMessenger in the widget tree
                  // and use it to show a SnackBar.
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                },
                child: const Text('Save'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {

      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have added thisf many apples:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

Card makeDashboardItem(String title, IconData icon) {
  return Card(
      elevation: 1.0,
      margin: EdgeInsets.all(8.0),
      child: Container(
        color: Colors.orange,
        child:  Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            Center(
                child: Icon(
                  icon,
                  size: 40.0,
                  color: Colors.black,
                )),
            Center(
              child:  Text(title,
                  style:
                  TextStyle(fontSize: 18.0, color: Colors.black)),
            )
          ],
        ),
      ));
}
